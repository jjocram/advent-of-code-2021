
def find_max_for_index(data, index):
    number_of_0 = 0
    number_of_1 = 0
    for d in data:
        if d[index] == "1":
            number_of_1 += 1
        else:
            number_of_0 += 1
    return number_of_0, number_of_1


def main():
    PATH_TO_DATAFILE = "../data/day3.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))

    o2 = data[:]
    co2 = data[:]
    for i in range(len(data[0])):
        if (len(o2) == 1):
            break
        number_of_0, number_of_1 = find_max_for_index(o2, i)
        if number_of_1 >= number_of_0:
            o2 = list(filter(lambda el: el[i] == "1", o2))
        else:
            o2 = list(filter(lambda el: el[i] == "0", o2))

    for i in range(len(data[0])):
        if (len(co2) == 1):
            break
        number_of_0, number_of_1 = find_max_for_index(co2, i)
        if number_of_1 >= number_of_0:
            co2 = list(filter(lambda el: el[i] == "0", co2))
        else:
            co2 = list(filter(lambda el: el[i] == "1", co2))

    print(int(o2[0], 2) * int(co2[0], 2))

if __name__ == "__main__":
    main()
