class BitStatus:
    number_of_0 = 0
    number_of_1 = 0

    def parse_bit(self, bit):
        if bit == "0":
            self.number_of_0 += 1
        else:
            self.number_of_1 += 1

    def get_rappresentation(self, data):
        if data == "gamma":
            if self.number_of_0 > self.number_of_1:
                return "0"
            else:
                return "1"
        else:
            if self.number_of_0 > self.number_of_1:
                return "1"
            else:
                return "0"

class Report:
    gamma = []
    epsilon = []

    def __init__(self, data_length):
        self.gamma = [BitStatus() for _ in range(data_length)]
        self.epsilon = [BitStatus() for _ in range(data_length)]

    def parse_input(self, data):
        values = list(data)
        for index, value in enumerate(values):
            self.gamma[index].parse_bit(value)
            self.epsilon[index].parse_bit(value)

    def multiplicate_gamma_epsilon(self):
        gamma_bits = [gb.get_rappresentation("gamma") for gb in self.gamma]
        epsislon_bits = [eb.get_rappresentation("epsilon") for eb in self.epsilon]
        return int("".join(gamma_bits), 2) * int("".join(epsislon_bits), 2)

def main():
    PATH_TO_DATAFILE = "../data/day3.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))
    r = Report(len(data[0]))
    for d in data:
        r.parse_input(d)
    print(r.multiplicate_gamma_epsilon())

if __name__ == "__main__":
    main()
