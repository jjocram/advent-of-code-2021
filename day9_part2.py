def is_min_wrt(el, el_index, row_index, data):
    if el_index < 0 or row_index < 0:
        #print("border -> T")
        return True
    try:
        #print(f"Comparing el: {el} with {data[row_index][el_index]}, is: ", end="")
        if el.value < data[row_index][el_index].value:
            #print("T")
            return True
        else:
            #print("F")
            return False
    except:
        #print("border -> T")
        return True

def is_local_min(el, el_index, row_index, data):
    to_ret = \
        is_min_wrt(el, el_index-1, row_index, data) and\
        is_min_wrt(el, el_index+1, row_index, data) and\
        is_min_wrt(el, el_index, row_index+1, data) and\
        is_min_wrt(el, el_index, row_index-1, data)
    #print(f"Element: {el} is {to_ret}")
    return to_ret

class Cell:
    def __init__(self, value) -> None:
        self.value = value
        self.visited = False

def find_neighbour_size(el_index, row_index, data):
    if row_index < 0 or el_index < 0:
        print("Left or upper border")
        return 0
    elif row_index >= len(data) or el_index >= len(data[0]):
        print("Right or down border")
        return 0
    elif data[row_index][el_index].value == 9:
        print("9 reached")
        return 0
    elif data[row_index][el_index].visited:
        print("cell already visited")
        return 0
    else:
        data[row_index][el_index].visited = True
        return 1 + \
            find_neighbour_size(el_index-1, row_index, data) +\
            find_neighbour_size(el_index+1, row_index, data) +\
            find_neighbour_size(el_index, row_index-1, data) +\
            find_neighbour_size(el_index, row_index+1, data)

def main():
    PATH_TO_DATAFILE = "../data/day9.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = [[Cell(int(value)) for value in list(line.replace("\n", ""))] for line in data_file.readlines()]
        basins_size = []
        for row_index, row in enumerate(data):
            for el_index, el, in enumerate(row):
                if is_local_min(el, el_index, row_index, data):
                    basins_size.append(find_neighbour_size(el_index, row_index, data))

        basins_size.sort()
        print(basins_size)
        print(basins_size[-1] * basins_size[-2] * basins_size[-3])

if __name__ == "__main__":
    main()
