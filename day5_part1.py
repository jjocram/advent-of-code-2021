def get_range(begin, end):
    if begin < end:
        return range(begin, end+1) # end+1 because range does not include last element
    else:
        return range(end, begin+1)


def main():
    PATH_TO_DATAFILE = "../data/day5.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))
    vents = {}
    for vent in data:
        begin, end = vent.split(" -> ")
        begin_x, begin_y = tuple(map(int, begin.split(",")))
        end_x, end_y = tuple(map(int, end.split(",")))
        if begin_x == end_x or begin_y == end_y:
            if begin_x == end_x:
                # Vertical vent => begin_y -> end_y
                for i in get_range(begin_y, end_y):
                    point = (begin_x, i)
                    if vents.get(point):
                        # A vent already exists
                        vents[point] += 1
                    else:
                        vents[point] = 1
            else:
                # Horizontal vent => begin_x -> end_y
                for i in get_range(begin_x, end_x):
                    point = (i, begin_y)
                    if vents.get(point):
                        # A vent already exists
                        vents[point] += 1
                    else:
                        vents[point] = 1

    overlap_points = list(filter(lambda el: vents[el]>1, vents))
    print(len(overlap_points))

if __name__ == "__main__":
    main()
