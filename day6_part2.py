from threading import Thread

def count_and_update(fishes, start_index, end_index):
    for index in range(start_index, end_index):
        if fishes[index] == 0:
                fishes.append(8)
                fishes[index] = 6
        else:
            fishes[index] = fishes[index] - 1



def main():
    PATH_TO_DATAFILE = "../data/day6.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        fishes = list(map(int, data_file.readline().split(",")))

    fish_counter = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 0,
    }

    for fish in fishes:
        fish_counter[fish] += 1

    for _ in range(256):
        #print(f"Working on day {day} of 255")
        old_fish_counter = dict(fish_counter)
        for new, old in zip(range(9), range(1, 9)):
            fish_counter[new] = old_fish_counter[old]

        # Update the last with the first
        fish_counter[8] = old_fish_counter[0]

        # Update the 6th with the 0s
        fish_counter[6] += old_fish_counter[0]

    print(sum(fish_counter.values()))

if __name__ == "__main__":
    main()
