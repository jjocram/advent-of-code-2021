# Advent of Code 2021
Data files are not provided. On my computer they are stored in `../data/dayX.txt`

## Notes
- Day 7 was resolved with [MiniZinc](https://www.minizinc.org/index.html)
- Day 8 part 2: solution inspired by an user on Reddit
