map_close_to_open = {
    ")" : "(",
    "]" : "[",
    "}" : "{",
    ">" : "<"
}

def find_corrupted_brackets(line):
    open_brackets = []
    for char in list(line):
        if char in {'(', '[', '{', '<'}:
            open_brackets.append(char)
            #print(f"Add open bracket -> {open_brackets}")
        else:
            last_open_bracket = open_brackets.pop()
            #print(f"{last_open_bracket} == {char}")
            if map_close_to_open[char] != last_open_bracket:
                #print(f"Wrong char {char} at index {ch_index} found in {line}")
                return False, None
    return True, open_brackets

map_bracket_value = {
    "(": 1,
    "[": 2,
    "{": 3,
    "<": 4
}

def main():
    PATH_TO_DATAFILE = "../data/day10.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))

    scores = []
    for line in data:
        to_keep, open_brackets = find_corrupted_brackets(line)
        score = 0
        if to_keep:
            for missing_bracket in list(reversed(open_brackets)):
                score *= 5
                score += map_bracket_value[missing_bracket]
            scores.append(score)

    scores.sort()
    print(scores[(len(scores) - 1)//2])

if __name__ == "__main__":
    main()
