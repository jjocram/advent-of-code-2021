def is_min_wrt(el, el_index, row_index, data):
    if el_index < 0 or row_index < 0:
        #print("border -> T")
        return True
    try:
        #print(f"Comparing el: {el} with {data[row_index][el_index]}, is: ", end="")
        if el < int(data[row_index][el_index]):
            #print("T")
            return True
        else:
            #print("F")
            return False
    except:
        print("border -> T")
        return True

def is_local_min(el, el_index, row_index, data):
    to_ret = \
        is_min_wrt(el, el_index-1, row_index, data) and\
        is_min_wrt(el, el_index+1, row_index, data) and\
        is_min_wrt(el, el_index, row_index+1, data) and\
        is_min_wrt(el, el_index, row_index-1, data)
    #print(f"Element: {el} is {to_ret}")
    return to_ret


def main():
    PATH_TO_DATAFILE = "../data/day9.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: list(el_str.replace("\n", "")),
                    data_file.readlines()))

        min_points = []
        for row_index, row in enumerate(data):
            for el_index, el, in enumerate(row):
                if is_local_min(int(el), el_index, row_index, data):
                    min_points.append(int(el))

        #print(min_points)
        print(sum(min_points)+len(min_points))


if __name__ == "__main__":
    main()
