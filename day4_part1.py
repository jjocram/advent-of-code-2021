class Cell:
    def __init__(self, value) -> None:
        self.value = int(value)
        self.marked = False

    def __repr__(self) -> str:
        return str(self.value)


class Board:
    def __init__(self, data) -> None:
        self.board = list()
        for line in data:
            line_cell = [Cell(value) for value in line]
            self.board.append(line_cell)


    def check_input_value(self, iv):
        for index_line, line in enumerate(self.board):
            for index_value, value in enumerate(line):
                if value.value == iv:
                    value.marked = True
                    bingo = True
                    for i in range(5):
                        if self.board[index_line][i].marked:
                            bingo &= True
                        else:
                            bingo &= False
                    if bingo:
                        return True

                    bingo = True
                    for i in range(5):
                        if self.board[i][index_value].marked:
                            bingo &= True
                        else:
                            bingo &= False
                    if bingo:
                        return True
        return False

    def __repr__(self) -> str:
        s = ""
        for line in self.board:
            for value in line:
                s += str(value) + " "
            s += "\n"
        return s

    def sum_unmarked(self):
        sum_unmarked = 0
        for line in self.board:
            for value in line:
                if not value.marked:
                    sum_unmarked += value.value
        return sum_unmarked

def check_boards_for_bingo(boards, input_value):
    for board in boards:
        if board.check_input_value(input_value):
            return board
    return None

def get_winning_board(boards, input_values):
    for iv in input_values:
        possible_board = check_boards_for_bingo(boards, iv)
        if possible_board:
            return possible_board, iv

    return None, None

def main():
    PATH_TO_DATAFILE = "../data/day4.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        input_values = list(map(lambda el: int(el), data_file.readline().split(",")))
        boards_values = [el.split() for el in data_file.readlines()[1:]] #[1:] skip the first empty line
        boards_values.append([]) #Add empty list at the end for an easy parse

    boards = list()
    board_values = list()
    for bv in boards_values:
        if bv == []:
            boards.append(Board(board_values))
            board_values = list()
        else:
            board_values.append(bv)

    first_winning_board, winning_value = get_winning_board(boards, input_values)
    print(f"First winning board with {winning_value}:\n", first_winning_board)
    print("Score: ", first_winning_board.sum_unmarked() * winning_value)


if __name__ == "__main__":
    main()
