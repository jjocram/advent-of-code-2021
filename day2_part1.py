class Position:
    horizontal = 0
    depth = 0

    def parse_command(self, command):
        operation, value_str = command.split()
        value = int(value_str)
        if operation == "forward":
            self.horizontal += value
        elif operation == "down":
            self.depth += value
        elif operation == "up":
            self.depth -= value
        else:
            print("Operation not permitted")


def compute_multiplication_horizontal_depth(data):
    position = Position()
    for el in data:
        position.parse_command(el)
    print(position.depth * position.horizontal)


def main():
    PATH_TO_DATAFILE = "../data/day2.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))
    compute_multiplication_horizontal_depth(data)


if __name__ == "__main__":
    main()
