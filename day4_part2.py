class Cell:
    def __init__(self, value) -> None:
        self.value = int(value)
        self.marked = False

    def __repr__(self) -> str:
        return str(self.value)

winning_counter = 0

class Board:
    def __init__(self, data) -> None:
        self.won = False
        self.winning_value = None
        self.board = list()
        self.this_winning_counter = None
        for line in data:
            line_cell = [Cell(value) for value in line]
            self.board.append(line_cell)


    def check_input_value(self, iv):
        global winning_counter
        for index_line, line in enumerate(self.board):
            for index_value, value in enumerate(line):
                if value.value == iv:
                    value.marked = True
                    bingo = True
                    for i in range(5):
                        if self.board[index_line][i].marked:
                            bingo &= True
                        else:
                            bingo &= False
                    if bingo:
                        self.won = True
                        self.winning_value = iv
                        self.this_winning_counter = winning_counter
                        winning_counter += 1
                        return True

                    bingo = True
                    for i in range(5):
                        if self.board[i][index_value].marked:
                            bingo &= True
                        else:
                            bingo &= False
                    if bingo:
                        self.won = True
                        self.winning_value = iv
                        self.this_winning_counter = winning_counter
                        winning_counter += 1
                        return True
        return False

    def __repr__(self) -> str:
        s = ""
        for line in self.board:
            for value in line:
                s += str(value) + " "
            s += "\n"
        return s

    def sum_unmarked(self):
        sum_unmarked = 0
        for line in self.board:
            for value in line:
                if not value.marked:
                    sum_unmarked += value.value
        return sum_unmarked

def check_boards_for_bingo(boards, input_value):
    for board in boards:
        if board.won:
            print(f"This board has already won with value {board.winning_value}\n{board}\n")
        else:
            board.check_input_value(input_value)

def get_winning_board(boards, input_values):
    for iv in input_values:
        check_boards_for_bingo(boards, iv)

    return max(list(filter(lambda b: b.won, boards)), key=lambda b: b.this_winning_counter)


def main():
    PATH_TO_DATAFILE = "../data/day4.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        input_values = list(map(lambda el: int(el), data_file.readline().split(",")))
        boards_values = [el.split() for el in data_file.readlines()[1:]] #[1:] skip the first empty line
        boards_values.append([]) #Add empty list at the end for an easy parse

    boards = list()
    board_values = list()
    for bv in boards_values:
        if bv == []:
            boards.append(Board(board_values))
            board_values = list()
        else:
            board_values.append(bv)

    last_winning_board = get_winning_board(boards, input_values)
    print("Score: ", last_winning_board.sum_unmarked() * last_winning_board.winning_value)


if __name__ == "__main__":
    main()
