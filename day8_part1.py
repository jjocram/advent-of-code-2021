from itertools import chain

def main():
    PATH_TO_DATAFILE = "../data/day8.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))

    output_values = [line.split("|")[1].split() for line in data]

    output_values_1d = list(chain.from_iterable(output_values))

    number_of_elements_of_unique_length = len(list(filter(lambda el: len(el) == 2 or len(el) == 4 or len(el) == 3 or len(el) == 7, output_values_1d)))
    print(number_of_elements_of_unique_length)


if __name__ == "__main__":
    main()
