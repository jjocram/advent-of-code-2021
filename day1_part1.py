def get_total_number_of_increase_in(data):
    total_number_of_increase = 0

    for index, el in enumerate(data):
        if index == 0:
            pass
        else:
            if el > data[index-1]:
                total_number_of_increase += 1

    return total_number_of_increase


def main():
    with open("../data/day1.txt", "r") as data_file:
        data = list(map(lambda el_str: int(el_str.replace("\n", "")),
                    data_file.readlines()))

    print(get_total_number_of_increase_in(data))


if __name__ == "__main__":
    main()
