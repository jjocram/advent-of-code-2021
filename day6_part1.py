def main():
    PATH_TO_DATAFILE = "../data/day6.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        fishes = list(map(int, data_file.readline().split(",")))

    for _ in range(80):
        for index in range(len(fishes)):
            if fishes[index] == 0:
                fishes.append(8)
                fishes[index] = 6
            else:
                fishes[index] = fishes[index] - 1
    print(len(fishes))

if __name__ == "__main__":
    main()
