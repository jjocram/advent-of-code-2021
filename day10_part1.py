map_close_to_open = {
    ")" : "(",
    "]" : "[",
    "}" : "{",
    ">" : "<"
}


def find_corrupted_brackets(line):
    open_brackets = []
    for char in list(line):
        if char in {'(', '[', '{', '<'}:
            open_brackets.append(char)
            #print(f"Add open bracket -> {open_brackets}")
        else:
            last_open_bracket = open_brackets.pop()
            #print(f"{last_open_bracket} == {char}")
            if map_close_to_open[char] != last_open_bracket:
                #print(f"Wrong char {char} at index {ch_index} found in {line}")
                return char
    return None

map_bracket_value = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137
}

def main():
    PATH_TO_DATAFILE = "../data/day10.txt"
    with open(PATH_TO_DATAFILE, "r") as data_file:
        data = list(map(lambda el_str: el_str.replace("\n", ""),
                    data_file.readlines()))

    wrong_brackets = list(filter(lambda el: el, [find_corrupted_brackets(line) for line in data]))
    values_from_wrong_brackets = list(map(lambda el: map_bracket_value[el], wrong_brackets))
    #print(wrong_brackets)
    print(sum(values_from_wrong_brackets))

if __name__ == "__main__":
    main()
